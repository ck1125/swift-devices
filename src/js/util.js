function deviceConfigAsJSON() {
	var deviceConfig = localStorage.getItem('swift_devices');
	var configString = "var deviceMapping = {";
	if (deviceConfig) {
		var config = JSON.parse(deviceConfig);
		for (var i = 0; i < config.devices.length; i++) {
			console.log(config.devices[i].mac+':'+config.devices[i].alias);
			if (i == (config.devices.length - 1)) {
				configString += '"'+config.devices[i].mac+'": "'+config.devices[i].alias+'"';
			} else {
				configString += '"'+config.devices[i].mac+'": "'+config.devices[i].alias+'",';
			}	
		}
	}	
		
	configString += "};"
	
	console.log(configString);
	return configString;	
}
