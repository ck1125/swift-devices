var IN3K8 = IN3K8 || {};

IN3K8.AdditionalLineItem = function(selector) {
	var self = this;
	self.source = selector;
	
	self.add  = function() {
		var node = $(self.source).parent();
		var index = $(node).data('device-id');
		var newIndex = index + 1;

		var nodeClone = $(node).clone(true,true);
		var inputFields = $(nodeClone).find('input');

		for (var i = 0; i < $(inputFields).length; i++) {
			var field = $(inputFields)[i];
			var fieldName = $(field).attr('name');
			$(field).attr('name',fieldName.replace(index,newIndex));
			$(field).val('');
		}
		var html = nodeClone.html();
		html = html.replace(new RegExp(index,'g'),newIndex);
		nodeClone.html(html);
		nodeClone.data('device-id',newIndex);
		$(self.source).remove();
		$(node).after($(nodeClone));
	
	};
    
}

IN3K8.AdditionalLineItem.prototype = {
	constructor: IN3K8.AdditionalLineItem
}
