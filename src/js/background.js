var pendingLoadId_ = null;
	
	
	function rewriteDom(tabId) {
		console.log('rewriting dom....');
		var config = deviceConfigAsJSON();
		chrome.tabs.executeScript(tabId,{file: 'js/jquery-1.8.3.min.js'},function() {
			chrome.tabs.executeScript(tabId,{code: config},function() {
				chrome.tabs.executeScript(tabId,{file: 'js/rewritedom.js'},function() {
					console.log('done executing scripts');
					console.log(arguments.length);
				});

			});

		});
	}
	
	/**
	 * Function runs on updating a tab having url of Swift profile.
	 * @param {integer} tabId Id of the tab which is updated.
	 * @param {String} changeInfo Gives the information of change in url.
	 * @param {String} tab Gives the url of the tab updated.
	 */
	function onTabUpdated(tabId, changeInfo, tab) {
	  var url = tab.url;
	  if (!url) {
	    return;
	  }

	  if (url.indexOf('www2.swiftng.com/Account/Profile') != -1)  {
	  	if (pendingLoadId_) {
			clearTimeout(pendingLoadId_);
		    pendingLoadId_ = null;
		}

		pendingLoadId_ = setTimeout(function() { rewriteDom(tabId) }, 10000);
	  }
	};


	/**
	 * Initializes everything.
	 */
	function init() {

	  chrome.tabs.onUpdated.addListener(onTabUpdated);
	};

	//Adding listener when body is loaded to call init function.
	window.addEventListener('load', init, false);
	
	console.log('extension loaded');