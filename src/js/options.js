$('.newline').live('click',function(e) {
	e.preventDefault();
	var addNewDeviceHandler = new IN3K8.AdditionalLineItem($(this));
	addNewDeviceHandler.add();	
});

$('.save').on('click',function(e) {
	e.preventDefault();
	var deviceAliases = [];
	var devices = $('div.device');
	for (var i = 0; i < devices.length; i++) {
		var currentDevice = i+1;
		var deviceMac =  $(devices[i]).find('input[name="mac'+currentDevice+'"]').val();
		var deviceAlias = $(devices[i]).find('input[name="deviceName'+currentDevice+'"]').val();
		
		deviceAliases.push({"mac" : deviceMac, "alias": deviceAlias});
	}
	// console.log(deviceAliases);
	localStorage.setItem('swift_devices',JSON.stringify({"devices" : deviceAliases}));
	console.log('saved');
});

var deviceConfig = localStorage.getItem('swift_devices');

if (deviceConfig) {
	var config = JSON.parse(deviceConfig);
	for (var i = 0; i < config.devices.length; i++) {
		currentDevice = i + 1;
		$('input[name="mac'+currentDevice+'"]').val(config.devices[i].mac);
		$('input[name="deviceName'+currentDevice+'"]').val(config.devices[i].alias);
		$('.newline').click();
	}
}
