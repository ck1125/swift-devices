describe('background', function() {
	
	it('should blank deviceMapping object when no configs',function() {
		var code = deviceConfigAsJSON();
		expect(code).toEqual('var deviceMapping = {};');
	});
	
	it('should deviceMapping object when configs exist',function() {
		var localStorageStub = sinon.stub(localStorage, 'getItem');
		localStorageStub.withArgs('swift_devices').returns('{"devices": [{"mac": "hello","alias":"hi"},{"mac": "hello1","alias":"hi1"} ]}');
		var code = deviceConfigAsJSON();
		expect(code).toEqual('var deviceMapping = {"hello": "hi","hello1": "hi1"};');
	});

});