module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	jasmine: {
	    customTemplate: {
	      src: 'src/**/*.js',
	      options: {
	        specs: 'test/**/*.js'
		  }
	    }
	  },
	 crx: {
	    deploy: {
	      "src": "src/",
	      "dest": "dist/<%= pkg.name %>-<%= pkg.version %>.crx",
	      "baseURL": "http://my.app.net/files/",
	      "exclude": [ ".git", ".svn" ],
	      "privateKey": "~/.ssh/swift_devices.pem",
	      "options": {
	        "maxBuffer": 3000 * 1024 //build extension with a weight up to 3MB
	      }
	    }
	  },
	 clean : {
	    folder : 'dist'
	  }
	
  });


  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-crx');

  grunt.registerTask('test', ['jasmine']);
  grunt.registerTask('package', ['crx']);

  grunt.registerTask('default', ['test']);

};