# Swift Devices #

[Swift](http://swiftng.com) a Nigerian ISP allows customers with a corporate account to attach multiple devices/modems to their account. 

One of the areas where their service is deficient is that even though you have physical names for your devices/modems either based on their installed location or any other business concern you are unable to reflect this name on your [account profile page](http://www2.swiftng.com/Account/Profile). This plugin allows you to do so very easily.
